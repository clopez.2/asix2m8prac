= Pràctica servidor correu electrònic (part 2)

//Taula de contingut
:doctype: article
:encoding: utf-8
:lang: ca
:toc: left
:toclevels: 3
:numbered:



== Apartat 6: Configuració d’un certificat SSL


.Entregar
====
image::images/MailP2Ex1.PNG[]
====

== Apartat 7: Configuració del Postfix - servei submission

.Entregar
====
image::images/MailP2Ex2.PNG[]
====

== Apartat 8: Configuració del Postfix - opcions generals

.Entregar
====
image::images/MailP2Ex3.PNG[]
====

.Entregar
====
image::images/MailP2Ex4.PNG[]
====

.Entregar
====
image::images/MailP2Ex5.PNG[]
====

.Entregar
====
image::images/MailP2Ex6.PNG[]
====

.Entregar
====
image::images/MailP2Ex7.PNG[]
====


