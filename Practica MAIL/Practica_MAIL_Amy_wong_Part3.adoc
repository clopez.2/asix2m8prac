= Pràctica servidor correu electrònic (part 3)

//Taula de contingut
:doctype: article
:encoding: utf-8
:lang: ca
:toc: left
:toclevels: 3
:numbered:



== Apartat 9: configuració dels àlies


.Entregar
====
image::images/MailP3Ex1.PNG[]
====

== Apartat 10: Configuració del protocol IMAP

.Entregar
====
image::images/MailP3Ex2.PNG[]
====

.Entregar
====
image::images/MailP3Ex3.PNG[]
====

.Entregar
====
image::images/MailP3Ex4.1.PNG[]
====

.Entregar
====
image::images/MailP3Ex4.2.PNG[]
====

.Entregar
====
image::images/MailP3Ex4.3.PNG[]
====

.Entregar
====
image::images/MailP3Ex5.PNG[]
====

.Entregar
====
image::images/MailP3Ex6.1.PNG[]
====

.Entregar
====
image::images/MailP3Ex6.2.PNG[]
====

.Entregar
====
image::images/MailP3Ex7.PNG[]
====

.Entregar
====
image::images/MailP3Ex8.PNG[]
====

.Entregar
====
image::images/MailP3Ex9.1.PNG[]
====

.Entregar
====
image::images/MailP3Ex9.2.PNG[]
====

.Entregar
====
image::images/MailP3Ex9.3.PNG[]
====

== Apartat 11: Configuració de Thunderbird

.Entregar
====
image::images/MailP3Ex10.1.PNG[]
====

.Entregar
====
image::images/MailP3Ex10.2.PNG[]
====

.Entregar
====
image::images/MailP3Ex10.3.PNG[]
====


